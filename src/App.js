import './App.css';
import LoginForm from './components/LoginForm/LoginForm';

function App() {
  
  return (
    <div className="bg-container">
      <div className='signup-container'>
        <div className='left-container'>
          <h1 className='percentage'>67%</h1>
          <span className='tag-line'> time saved in total compared to projects not using Hub.</span>
          <p>-fintory.com</p>
        </div>

        <div className='right-container'>
          <LoginForm />
        </div>
      </div>
    </div>
  );
}

export default App;
