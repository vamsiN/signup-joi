import React from 'react'
import './LoginForm.css'
import Joi from 'joi-browser';
import Form from '../Form/Form'

class LoginForm extends Form {
    state = {
        data: {
            firstname: "",
            lastname: "",
            gender: '',
            age: '',
            email: '',
            password: '',
            confirmPassword: '',
            role: '',
            tos: ''
        },
        errors: {},
    }

    schema = {
        firstname: Joi.string().required().label('Firstname'),
        lastname: Joi.string().required().label('Lastname'),
        gender: Joi.string().required().label('Gender'),
        age: Joi.number().integer().min(16).max(100).label('Age'),
        email: Joi.string().required().label('Email'),
        password: Joi.string().min(3).max(15).required().label('Password'),
        confirmPassword: Joi.string().required().label('Confirm Password'),
        role: Joi.string().required().label('Role'),
        tos: Joi.string().required().label('Tos')

    }

    doSubmit = () => {
        //call the server
        console.log("submitted")
    }

    render() {
        return (
            <div>
                <h1 className='signup-heading'>Signup</h1>
                <form className='form-container' onSubmit={this.handleSubmit}>
                    {this.renderInput('firstname', 'Firstname')}
                    {this.renderInput('lastname', 'Lastname')}
                    {this.renderInput('gender', 'Gender')}
                    {this.renderInput('age', 'Age')}
                    {this.renderInput('email', 'Email')}
                    {this.renderInput('password', 'Password', "password")}
                    {this.renderInput('confirmPassword', 'Confirm Password', "password")}
                    <div className='select-container' >
                        <select className='select-feild' name="role" label='Role'id="role" >
                            <option className='seletct-option' defaultValue="select" >--- Select your Role  ---</option>
                            <option className='seletct-option' value="developer">Developer</option>
                            <option className='seletct-option' value="senior developer">Senior Developer</option>
                            <option className='seletct-option' value="lead engineer">Lead Engineer</option>
                            <option className='seletct-option' value="cto">CTO</option>
                        </select>
                    </div>

                    {/* {this.renderInput('role', 'Role',)} */}
                    <div className='checkbox-container'>
                        <input className='checkbox' type='checkbox' name='tos' label='Tos' /> <span className='tos-text'>Agree to terms and conditions</span>
                    </div>
                    <br />
                    <div className='button-container'>
                        {this.renderButton('Sign Up')}
                    </div>


                </form>
            </div>
        )
    }



}
export default LoginForm;