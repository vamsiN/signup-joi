import React from 'react'
import './Input.css'

const Input = ({name, label, error, ...rest}) =>{
    return(
        <div className='input-container'>
            <label className='input-label' htmlFor={name}>{label}</label>
            <br />
            <input 
                {...rest}
                id={name}
                name={name}
                className='input-feild'
            />
            {error && <div className='input-error'>{error}</div>}
        </div>
    )
}
export default Input